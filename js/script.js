let texto = '';
let patron = '';
let pasoAPaso;
let contenedorTextoSpan;
let contenedorPatronSpan;
let posicionesCoincidencias = [];
let longitudTexto;
let longitudPatron;
let tablaDesplazamiento;
let indiceTexto;
let indicePatron;

function iniciarBusqueda () {

    texto = document.getElementById('texto').value.trim();
    patron = document.getElementById('patron').value.trim();
    pasoAPaso = document.getElementById('pasoAPaso');
    pasoAPaso.innerHTML = '';
    posicionesCoincidencias = [];
    longitudTexto = texto.length;
    longitudPatron = patron.length;

    if (longitudPatron > longitudTexto) {

        alert('El patrón debe ser más corto que el texto.');
        return;

    }

    if (longitudTexto === 0 || longitudPatron === 0) {

        alert('Tanto el texto como el patron deben tener contenido.');
        return;

    }

    tablaDesplazamiento = calcularTablaDesplazamiento(patron, longitudPatron);
    indiceTexto = 0;
    indicePatron = longitudPatron - 1;

    contenedorTextoSpan = crearElementosSpan(texto, 'texto');
    contenedorPatronSpan = crearElementosSpan(patron, 'patron');
    pasoAPaso.appendChild(contenedorTextoSpan);
    pasoAPaso.appendChild(contenedorPatronSpan);

    document.getElementById('siguiente').style.display = 'block';
    actualizarResultado();

}

function buscarCompleto () {

    iniciarBusqueda();

    while (indiceTexto <= longitudTexto - longitudPatron) {

        siguientePaso();

    }

    document.getElementById('siguiente').style.display = 'none';

}

function calcularTablaDesplazamiento (patron, longitud) {

    let tabla = new Array(256).fill(longitud);

    for (let i = 0; i < longitud - 1; i++) {

        tabla[patron.charCodeAt(i)] = longitud - i - 1;

    }

    return tabla;

}

function crearElementosSpan (contenido, clase) {

    const contenedorSpan = document.createElement('div');
    contenedorSpan.classList.add(clase);

    for (let char of contenido) {

        const span = document.createElement('span');
        span.textContent = char;
        contenedorSpan.appendChild(span);

    }

    return contenedorSpan;

}

function actualizarResultado () {

    const resultado = document.getElementById('resultado');
    resultado.textContent = `Coincidencias encontradas en las posiciones: ${posicionesCoincidencias.join(', ')}`;

}

function siguientePaso () {

    if (indiceTexto > longitudTexto - longitudPatron) {

        document.getElementById('siguiente').style.display = 'none';
        return;

    }

    resetearColores();

    if (indicePatron >= 0 && patron[indicePatron] === texto[indiceTexto + indicePatron]) {

        resaltarCaracteres('lightblue');
        indicePatron--;

    }else {

        manejarDesajuste();

    }

    actualizarResultado();

}

function resetearColores () {

    for (let i = 0; i < longitudPatron; i++) {

        if (indiceTexto + i < longitudTexto) {

            contenedorTextoSpan.children[indiceTexto + i].style.backgroundColor = '';

        }

        contenedorPatronSpan.children[i].style.backgroundColor = '';

    }

}

function resaltarCaracteres (color) {

    contenedorTextoSpan.children[indiceTexto + indicePatron].style.backgroundColor = color;
    contenedorPatronSpan.children[indicePatron].style.backgroundColor = color;

}

function manejarDesajuste () {

    if (indicePatron >= 0) {

        resaltarCaracteres('lightcoral');

    }

    if (indicePatron < 0) {

        for (let i = 0; i < longitudPatron; i++) {

            contenedorTextoSpan.children[indiceTexto + i].style.backgroundColor = 'yellow';

        }

        posicionesCoincidencias.push(indiceTexto);
        indiceTexto += longitudPatron;

    }else {

        indiceTexto += tablaDesplazamiento[texto.charCodeAt(indiceTexto + longitudPatron - 1)];

    }

    indicePatron = longitudPatron - 1;

}
